package hu.braininghub.bh06.hrr.dao.generator;

public class RandomNumberGenerationStrategy implements IdGenerationStrategy<Integer> {

	private Integer upperLimit;

	public static final Integer DEFAULT_RANDOM_GENERATION_UPPER_LIMIT = 1000000;

	public RandomNumberGenerationStrategy() {
		this(DEFAULT_RANDOM_GENERATION_UPPER_LIMIT);
	}

	public RandomNumberGenerationStrategy(Integer upperLimit) {

		if (upperLimit == null) {
			throw new IllegalArgumentException("Upper limit parameter is mandatory!");
		}

		if (upperLimit.intValue() < 1 || upperLimit.intValue() > DEFAULT_RANDOM_GENERATION_UPPER_LIMIT) {
			throw new IllegalArgumentException(
					"Upper limist must between 1 and " + DEFAULT_RANDOM_GENERATION_UPPER_LIMIT);
		}
		
		this.upperLimit = upperLimit;
	}

	@Override
	public Integer generateId() {
		return ((int) (Math.random() * upperLimit + 1));
	}
}
