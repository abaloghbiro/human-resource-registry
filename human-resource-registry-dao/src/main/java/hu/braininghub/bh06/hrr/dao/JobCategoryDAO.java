package hu.braininghub.bh06.hrr.dao;

import hu.braininghub.bh06.hrr.model.JobCategory;

public interface JobCategoryDAO extends BaseDAO<String, JobCategory> {

}
