package hu.braininghub.bh06.hrr.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import hu.braininghub.bh06.hrr.dao.JobDAO;
import hu.braininghub.bh06.hrr.dao.generator.IdGenerationStrategy;
import hu.braininghub.bh06.hrr.dao.generator.UUIDGenerationStrategy;
import hu.braininghub.bh06.hrr.model.Job;

public class DefaultJobDAO extends DefaultBaseDAO<String, Job> implements JobDAO {

	
	@Override
	public IdGenerationStrategy<String> getIDGenerationStrategy() {
		return new UUIDGenerationStrategy();
	}

	@Override
	public Job getJobByTitleAndDepartmentName(String jobTitle, String departmentName) {

		List<Job> jobs = entities.values().stream()
				.filter(j -> j.getTitle().equals(jobTitle) && j.getDepartment().getName().equals(departmentName))
				.collect(Collectors.toList());

		return jobs.get(0);
	}

}
