package hu.braininghub.bh06.hrr.dao.impl;

import hu.braininghub.bh06.hrr.dao.EmployeeDAO;
import hu.braininghub.bh06.hrr.dao.generator.IdGenerationStrategy;
import hu.braininghub.bh06.hrr.dao.generator.UUIDGenerationStrategy;
import hu.braininghub.bh06.hrr.model.Employee;

public class DefaultEmployeeDAO extends DefaultBaseDAO<String, Employee> implements EmployeeDAO {

	@Override
	public IdGenerationStrategy<String> getIDGenerationStrategy() {
		return new UUIDGenerationStrategy();
	}

}
