package hu.braininghub.bh06.hrr.dao;

import hu.braininghub.bh06.hrr.model.Employee;

public interface EmployeeDAO extends BaseDAO<String, Employee> {

}
