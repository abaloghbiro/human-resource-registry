package hu.braininghub.bh06.hrr.dao;

import hu.braininghub.bh06.hrr.model.Department;

public interface DepartmentDAO extends BaseDAO<String, Department> {

	Department getDepartmentByName(String departmentName);
}
