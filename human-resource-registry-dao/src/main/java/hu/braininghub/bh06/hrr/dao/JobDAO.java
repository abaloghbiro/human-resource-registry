package hu.braininghub.bh06.hrr.dao;

import hu.braininghub.bh06.hrr.model.Job;

public interface JobDAO extends BaseDAO<String, Job> {

	Job getJobByTitleAndDepartmentName(String jobTitle, String departmentName);
}
