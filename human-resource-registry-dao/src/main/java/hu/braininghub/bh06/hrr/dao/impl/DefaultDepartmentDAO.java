package hu.braininghub.bh06.hrr.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import hu.braininghub.bh06.hrr.dao.DepartmentDAO;
import hu.braininghub.bh06.hrr.dao.generator.IdGenerationStrategy;
import hu.braininghub.bh06.hrr.dao.generator.UUIDGenerationStrategy;
import hu.braininghub.bh06.hrr.model.Department;

public class DefaultDepartmentDAO extends DefaultBaseDAO<String, Department> implements DepartmentDAO {

	@Override
	public IdGenerationStrategy<String> getIDGenerationStrategy() {
		return new UUIDGenerationStrategy();
	}

	@Override
	public Department getDepartmentByName(String departmentName) {

		Department ret = null;

		List<Department> departments = entities.values().stream()
				.filter(d -> d.getName()
						.equalsIgnoreCase(departmentName)).collect(Collectors.toList());

		if (departments == null || departments.isEmpty() || departments.size() > 1) {
			throw new IllegalArgumentException("Department not found or it is ambigous!");
		}
		
		ret = departments.get(0);
		return ret;
	}

}
