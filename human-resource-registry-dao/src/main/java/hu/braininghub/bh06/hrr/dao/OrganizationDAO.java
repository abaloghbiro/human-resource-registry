package hu.braininghub.bh06.hrr.dao;

import hu.braininghub.bh06.hrr.model.Organization;

public interface OrganizationDAO extends BaseDAO<String,Organization> {

	Organization getOrganizationByName(String name);
}
