package hu.braininghub.bh06.hrr.dao;

import hu.braininghub.bh06.hrr.model.JobHistory;

public interface JobHistoryDAO extends BaseDAO<String, JobHistory> {

}
