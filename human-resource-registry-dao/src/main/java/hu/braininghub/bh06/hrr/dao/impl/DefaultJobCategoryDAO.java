package hu.braininghub.bh06.hrr.dao.impl;

import hu.braininghub.bh06.hrr.dao.JobCategoryDAO;
import hu.braininghub.bh06.hrr.dao.generator.IdGenerationStrategy;
import hu.braininghub.bh06.hrr.dao.generator.UUIDGenerationStrategy;
import hu.braininghub.bh06.hrr.model.JobCategory;

public class DefaultJobCategoryDAO extends DefaultBaseDAO<String, JobCategory> implements JobCategoryDAO {

	@Override
	public IdGenerationStrategy<String> getIDGenerationStrategy() {
		return new UUIDGenerationStrategy();
	}

}
