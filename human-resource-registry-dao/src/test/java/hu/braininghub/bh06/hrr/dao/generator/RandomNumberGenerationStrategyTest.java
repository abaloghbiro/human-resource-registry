package hu.braininghub.bh06.hrr.dao.generator;

import static hu.braininghub.bh06.hrr.dao.generator.RandomNumberGenerationStrategy.DEFAULT_RANDOM_GENERATION_UPPER_LIMIT;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RandomNumberGenerationStrategyTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Test
	public void testRandomNumberGeneratedSuccessfullyWithIntervall1000() {

		IdGenerationStrategy<Integer> s = new RandomNumberGenerationStrategy(1000);

		Integer generatedId = s.generateId();

		assertNotNull("Generated Id cannot be null!", generatedId);

		boolean generatedNumberIsInsideTheIntervall = generatedId >= 1 && generatedId <= 1000;

		if (!generatedNumberIsInsideTheIntervall) {
			fail("Generated id is not fit to the specified intervall 1 - 1000");
		}
		assertTrue(generatedNumberIsInsideTheIntervall);

	}

	@Test
	public void testRandomNumberGenerationFailedWithNullParam() {

		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Upper limit parameter is mandatory!");
		
		new RandomNumberGenerationStrategy(null);

	}

	@Test
	public void testRandomNumberGenerationFailedWithNegativeUpperLimit() {

		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Upper limist must between 1 and "
				+ DEFAULT_RANDOM_GENERATION_UPPER_LIMIT);
		
		new RandomNumberGenerationStrategy(-1);

	}

	@Test
	public void testRandomNumberGenerationFailedParameterGreaterThenUpperLimit() {

		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Upper limist must between 1 and "
				+ DEFAULT_RANDOM_GENERATION_UPPER_LIMIT);
		
		new RandomNumberGenerationStrategy(DEFAULT_RANDOM_GENERATION_UPPER_LIMIT + 1);

	}
	
	@Test
	public void testRandomNumberGeneratedSuccessfullyWithDefaultConstructor() {

		IdGenerationStrategy<Integer> s = new RandomNumberGenerationStrategy();

		Integer generatedId = s.generateId();

		assertNotNull("Generated Id cannot be null!", generatedId);

		boolean generatedNumberIsInsideTheIntervall = generatedId >= 1 && generatedId <= DEFAULT_RANDOM_GENERATION_UPPER_LIMIT;

		assertTrue(generatedNumberIsInsideTheIntervall);

	}
}
