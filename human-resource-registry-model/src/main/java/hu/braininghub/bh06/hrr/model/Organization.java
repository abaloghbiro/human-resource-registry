package hu.braininghub.bh06.hrr.model;

public class Organization extends BusinessObject<String> {

	private String organizationName;

	private int foundationYear;

	private String hqAddress;

	private String primaryPhoneNumber;

	
	public Organization(String id, int foundationYear) {
		super(id);
		this.foundationYear = foundationYear;
	}
	
	public int getFoundationYear() {
		return foundationYear;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getHqAddress() {
		return hqAddress;
	}

	public void setHqAddress(String hqAddress) {
		this.hqAddress = hqAddress;
	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}
	
}
