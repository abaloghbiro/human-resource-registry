package hu.braininghub.bh06.hrr.model;

public class JobCategory extends BusinessObject<String> {

	private String name;
	private Integer minSalary;
	private Integer maxSalary;
	private Integer level;

	public JobCategory(String id, String name, Integer level) {
		super(id);
		this.name = name;
		this.level = level;
		this.active = true;
	}

	public Integer getMinSalary() {
		return minSalary;
	}

	public void setMinSalary(Integer minSalary) {
		this.minSalary = minSalary;
	}

	public Integer getMaxSalary() {
		return maxSalary;
	}

	public void setMaxSalary(Integer maxSalary) {
		this.maxSalary = maxSalary;
	}
	
	public String getName() {
		return name;
	}

	public Integer getLevel() {
		return level;
	}

}
