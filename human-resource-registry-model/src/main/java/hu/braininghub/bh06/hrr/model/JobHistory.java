package hu.braininghub.bh06.hrr.model;

public class JobHistory extends BusinessObject<String> {

	private String fromDate;
	private String toDate;
	private Job job;
	
	public JobHistory(String id,String fromDate, Job job) {
		super(id);
		this.fromDate = fromDate;
		this.job = job;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getFromDate() {
		return fromDate;
	}

	public Job getJob() {
		return job;
	}

	@Override
	public String toString() {
		return "JobHistory [fromDate=" + fromDate + ", toDate=" + toDate + ", job=" + job + "]";
	}

	
	
}
